#include <cstdio>
#include "util.hpp"

void GS::Clear::screen() { printf("\x1b[J"); }
void GS::Clear::screenLeft() { printf("\x1b[0J"); }
void GS::Clear::screenRight() { printf("\x1b[1J"); }
void GS::Clear::entireScreen() { printf("\x1b[2J"); }
void GS::Clear::line() { printf("\x1b[K"); }
void GS::Clear::lineLeft() { printf("\x1b[0K"); }
void GS::Clear::lineRight() { printf("\x1b[1K"); }
void GS::Clear::entireLine() { printf("\x1b[2K"); }

void GS::Cursor::home() { printf("\x1b[H"); }
void GS::Cursor::moveTo(int x, int y) { printf("\x1b[%d,%dH", y, x); }
void GS::Cursor::moveTo(int x) { printf("\x1b[%dG", x); }
void GS::Cursor::up(int y) { printf("\x1b[%dA", y); }
void GS::Cursor::down(int y) { printf("\x1b[%dB", y); }
void GS::Cursor::right(int x) { printf("\x1b[%dC", x); }
void GS::Cursor::left(int x) { printf("\x1b[%dD", x); }
void GS::Cursor::nextLine(int n) { printf("\x1b[%dE", n); }
void GS::Cursor::prevLine(int n) { printf("\x1b[%dF", n); }
void GS::Cursor::save() { printf("\x1b[s"); }
void GS::Cursor::load() { printf("\x1b[u"); }

int modes[] = { 1, 2, 3, 4, 5, 7, 8, 9 };

std::string GS::Mode::string(uint8_t mode) {
    std::string result = "\x1b[";
    bool hasMode = false;
    for(uint8_t i = 0; i < 8; i++) {
        if (mode & (1 << i)) {
            if (hasMode) result += ",";
            result += std::to_string(modes[i]);
            hasMode = true;
        }
    }
    if (!hasMode) {
        result += "0";
    }
    return result + "m";
}

void GS::Mode::set(uint8_t mode) {
    printf("%s", GS::Mode::string(mode).c_str());
}
