#include <string>
#include <cstdint>

namespace GS {
    namespace Clear {
        void screen();
        void screenLeft();
        void screenRight();
        void entireScreen();
        void line();
        void lineLeft();
        void lineRight();
        void entireLine();
    }
    namespace Cursor {
        void home();
        void moveTo(int x, int y);
        void moveTo(int x);
        void up(int y = 1);
        void down(int y = 1);
        void right(int x = 1);
        void left(int x = 1);
        void nextLine(int n = 1);
        void prevLine(int n = 1);
        void save();
        void load();
    }
    namespace Mode {
        void set(uint8_t mode = 0);
        std::string string(uint8_t mode = 0);
        constexpr uint8_t
            BOLD =      0b00000001,
            DIM =       0b00000010,
            ITALIC =    0b00000100,
            UNDERLINE = 0b00001000,
            BLINK =     0b00010000,
            INVERSE =   0b00100000,
            INVISIBLE = 0b01000000,
            STRIKE =    0b10000000;
    }
}
