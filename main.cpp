#include <cstdio>

#include "util.hpp"

using namespace GS;

int main(int, char**) {
    Cursor::moveTo(5, 5);
    Mode::set(Mode::BLINK | Mode::ITALIC | Mode::BOLD | Mode::UNDERLINE);
    printf("Hello, World!");
    Cursor::nextLine(3);
}
